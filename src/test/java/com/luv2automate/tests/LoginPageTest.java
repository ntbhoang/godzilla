package com.luv2automate.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class LoginPageTest {

    // First Principle: Never hard code

    @Test
    public void test1(){
        System.setProperty("webdriver.chrome.driver",
                System.getProperty("user.dir") + "/src/test/resources/executableDrivers/chromedriver");
        System.setProperty("webdriver.chrome.whitelistedIps", "127.0.0.1");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.get("https://www.google.com.vn/");
        driver.findElement(By.name("q")).sendKeys("Automation", Keys.ENTER);
        driver.quit();
    }

    @Test
    public void test2(){
        System.setProperty("webdriver.chrome.driver",
                System.getProperty("user.dir") + "/src/test/resources/executableDrivers/chromedriver");
        System.setProperty("webdriver.chrome.whitelistedIps", "127.0.0.1");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.get("https://www.google.com.vn/");
        driver.findElement(By.name("q")).sendKeys("Cypress Automation JS web page", Keys.ENTER);
        driver.quit();
    }
}
